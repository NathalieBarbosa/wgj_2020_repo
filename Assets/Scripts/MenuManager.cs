﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{

    [SerializeField] GameObject[] objectDeactivate;
    [SerializeField] bool isOpenOpt;
    // Start is called before the first frame update
    void Start()
    {
        isOpenOpt = false;
    }

    // Update is called once per frame
    public void ExitBtn()
    {
        Application.Quit();
    }

    public void openOptionsMenu()
    { 
         isOpenOpt = true;

        for (int i = 0; i < objectDeactivate.Length; i++)
        {
          
            objectDeactivate[i].SetActive(false);
               
        }
    }

    public void MainMenuBtn()
    {
        SceneManager.LoadScene("MainMenu");
    }
    public void PlayBtn()
    {
        SceneManager.LoadScene("Level1");
    }

}
